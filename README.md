<h1>Desafio Automação de Testes - API</h1>

Neste repositório você irá encontrar as respostas para o desafio de automação de testes de api de API para o SICREDI.
Os testes foram divididos em duas partes:

<h3>Os testes foram divididos em duas partes:</h3>

<li><a href="https://gitlab.com/brunostreit/desafio_api_sicredi/-/blob/main/desafio_api_sicredi/src/main/java/desafio_api_sicredi/ConsultaRestricaoCPF.java" rel="nofollow">Testes de Restrições</a>



<li><a href="https://gitlab.com/brunostreit/desafio_api_sicredi/-/blob/main/desafio_api_sicredi/src/main/java/desafio_api_sicredi/Simulacao.java" rel="nofollow">Testes de Simulações</a>


<h3>Observações:</h3>
<li>Para alterar a url base de execução, basta modificar o arquivo Constantes

<h3>Problemas encontrados</h3>
Durante o desenvolvimento dos testes foram encontrados alguns problemas que seguem listados abaixo:

<h6>1-EXCLUSÃO DE SIMULAÇÃO (DELETE)</h6>
No documento de orientações para a execução, informa que após realizar a exclusão de sucesso, deveria retornar 204. Está retornando código 200.

No documento de orientações para a execução, informa que se não existir simulação para o id informado deveria retornar 404. Está retornando 200 com a mensagem "OK".

<h6>2-CRIAÇÃO</h6>
No documento de orientações para a execução, informa que ao criar uma simulação para um mesmo CPF deveria retornar 409, porém retorna 400 com a mensagem "CPF duplicado".

<h6>3-REGRAS</h6>
No documento de orientações para a execução, informa que não deveria permitir valor abaixo de 1000. Está permitindo cadastrar valores abaixo de 1000.

No documento de orientações para a execução, informa que não deveria permitir parcelas acima de 48. Está permitindo cadastrar parcelas acima de 48.


 


