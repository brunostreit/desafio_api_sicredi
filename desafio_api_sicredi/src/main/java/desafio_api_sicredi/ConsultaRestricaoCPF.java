package desafio_api_sicredi;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;
import org.junit.Test;

import desafio_sicredi_core.BaseTest;

public class ConsultaRestricaoCPF extends BaseTest {
	@Test
	public void consulta97093236014() {
		given()
		.when()
			.get("/restricoes/97093236014")
		.then()
			.body("mensagem", is("O CPF 97093236014 tem problema"))
			.statusCode(200);
	}
	
	@Test
	public void consulta60094146012() {
		given()
		.when()
			.get("/restricoes/60094146012")
		.then()
			.body("mensagem", is("O CPF 60094146012 tem problema"))
			.statusCode(200);
	}
	
	@Test
	public void consulta84809766080() {
		given()
		.when()
			.get("/restricoes/84809766080")
		.then()
			.body("mensagem", is("O CPF 84809766080 tem problema"))
			.statusCode(200);
	}
	
	@Test
	public void consulta62648716050() {
		given()
		.when()
			.get("/restricoes/62648716050")
		.then()
			.body("mensagem", is("O CPF 62648716050 tem problema"))
			.statusCode(200);
	}
	
	@Test
	public void consulta26276298085() {
		given()
		.when()
			.get("/restricoes/26276298085")
		.then()
			.body("mensagem", is("O CPF 26276298085 tem problema"))
			.statusCode(200);
	}
	
	@Test
	public void consulta01317496094() {
		given()
		.when()
			.get("/restricoes/01317496094")
		.then()
			.body("mensagem", is("O CPF 01317496094 tem problema"))
			.statusCode(200);
	}
	
	@Test
	public void consulta55856777050() {
		given()
		.when()
			.get("/restricoes/55856777050")
		.then()
			.body("mensagem", is("O CPF 55856777050 tem problema"))
			.statusCode(200);
	}
	
	@Test
	public void consulta19626829001() {
		given()
		.when()
			.get("/restricoes/19626829001")
		.then()
			.body("mensagem", is("O CPF 19626829001 tem problema"))
			.statusCode(200);
	}
	
	@Test
	public void consulta24094592008() {
		given()
		.when()
			.get("/restricoes/24094592008")
		.then()
			.body("mensagem", is("O CPF 24094592008 tem problema"))
			.statusCode(200);
	}
	
	@Test
	public void consulta58063164083() {
		given()
		.when()
			.get("/restricoes/58063164083")
		.then()
			.body("mensagem", is("O CPF 58063164083 tem problema"))
			.statusCode(200);
	}
	
	@Test
	public void consulta26353778120() {
		given()
		.when()
			.get("/restricoes/26353778120")
		.then()
			.statusCode(204);
	}
	
}
