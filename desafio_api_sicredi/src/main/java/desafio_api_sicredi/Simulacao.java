package desafio_api_sicredi;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import desafio_sicredi_core.BaseTest;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class Simulacao extends BaseTest{
	
	private static Integer ID;
	
	@Test
	public void t01_criarSimulacao() {
     ID=given()
			.contentType("application/json")
			.body("{\"nome\": \"Simulacao Bruno\", \"cpf\": 19960888070, \"email\": \"email@email.com\", \"valor\": 1300, \"parcelas\": 3, \"seguro\": true}")
		.when()
			.post("/simulacoes")
		.then()
			.body("id", is(notNullValue()))
			.body("nome", is("Simulacao Bruno"))
			.body("cpf", is("19960888070"))
			.body("email", is("email@email.com"))
			.body("valor", is(1300))
			.body("parcelas", is(3))
			.body("seguro", is(true))
			.statusCode(201)
		    .extract().path("id");
	}
	
	@Test
	public void t02_criarSimulacaoCPFFormatoErrado() {
        given()
			.contentType("application/json")
			.body("{\"nome\": \"Simulacao Bruno\", \"cpf\": 199.608.880-70, \"email\": \"email@email.com\", \"valor\": 1300, \"parcelas\": 3, \"seguro\": true}")
		.when()
			.post("/simulacoes")
		.then()
			.statusCode(400);
	}
	
	@Test
	public void t03_criarSimulacaoNomeFormatoErrado() {
        given()
			.contentType("application/json")
			.body("{\"nome\": \"111111+_*\", \"cpf\": 19960888070, \"email\": \"email@email.com\", \"valor\": 1300, \"parcelas\": 3, \"seguro\": true}")
		.when()
			.post("/simulacoes")
		.then()
			.statusCode(400);
	}
	
	@Test
	public void t04_criarSimulacaoEmailFormatoErrado() {
        given()
			.contentType("application/json")
			.body("{\"nome\": \"Simulacao Bruno\", \"cpf\": 19960888070, \"email\": \"emailemail.com\", \"valor\": 1300, \"parcelas\": 3, \"seguro\": true}")
		.when()
			.post("/simulacoes")
		.then()
			.statusCode(400)
            .body(containsString("não é um endereço de e-mail"));
	}
	
	@Test
	public void t05_criarSimulacaoValorErradoAcima() {
        given()
			.contentType("application/json")
			.body("{\"nome\": \"Simulacao Bruno\", \"cpf\": 19960888070, \"email\": \"email@email.com\", \"valor\": 40001, \"parcelas\": 3, \"seguro\": true}")
		.when()
			.post("/simulacoes")
		.then()
			.statusCode(400)
            .body(containsString("Valor deve ser menor ou igual a R$ 40.000"));
	}
	
	@Test
	public void t06_criarSimulacaoParcelaErradoAbaixo() {
        given()
			.contentType("application/json")
			.body("{\"nome\": \"Simulacao Bruno\", \"cpf\": 19960888070, \"email\": \"email@email.com\", \"valor\": 1300, \"parcelas\": 1, \"seguro\": true}")
		.when()
			.post("/simulacoes")
		.then()
			.statusCode(400)
            .body(containsString("Parcelas deve ser igual ou maior que 2"));
	}
	
	@Test
	public void t07_criarSimulacaoExistente() {
        given()
			.contentType("application/json")
			.body("{\"nome\": \"Teste 2\", \"cpf\": 19960888070, \"email\": \"email@email.com\", \"valor\": 1300, \"parcelas\": 3, \"seguro\": true}")
		.when()
			.post("/simulacoes")
		.then()
			//aqui deveria retornar 409
			.statusCode(400)
            .body(containsString("CPF duplicado"));
	}
	
	@Test
	public void t08_criarSimulacaoProblema() {
        given()
			.contentType("application/json")
			.body("{\"nome\": \"\", \"cpf\": , \"email\": \"\", \"valor\": , \"parcelas\": , \"seguro\": }")
		.when()
			.post("/simulacoes")
		.then()
			.statusCode(400);
	}
	
	@Test
	public void t09_consultarTodasSimulacoes() {
        given()
		.when()
			.get("/simulacoes")
		.then()
			.body("id", is(notNullValue()))
			.statusCode(200);
	}
	
	@Test
	public void t10_alterarSimulacao() {
        given()
			.contentType("application/json")
			.body("{\"nome\": \"Simulacao Bruno Alterado\", \"cpf\": 19960888070, \"email\": \"email_alterado@email.com\", \"valor\": 1300, \"parcelas\": 2, \"seguro\": false}")
		.when()
			.put("/simulacoes/19960888070")
		.then()
			.body("id", is(notNullValue()))
		.statusCode(200);
	}
	
	@Test
	public void t11_consultarSimulacaoAlterada() {
        given()
        .when()
			.get("/simulacoes/19960888070")
		.then()
			.body("id", is(notNullValue()))
			.body("nome", is("Simulacao Bruno Alterado"))
			.body("cpf", is("19960888070"))
			.body("email", is("email_alterado@email.com"))
			.body("parcelas", is(2))
			.body("seguro", is(false))
			.statusCode(200);
	}
	
	@Test
	public void t12_consultarSimulacaoInexistente() {
        given()
		.when()
			.get("/simulacoes/85913471296")
		.then()
     		//aqui deveria retornar 204
			.statusCode(404);
	}
	
	@Test
	public void t13_alterarSimulacaoNaoEncontrado() {
        given()
			.contentType("application/json")
			.body("{\"nome\": \"Simulacao Bruno Alterado\", \"cpf\": 19960888070, \"email\": \"email_alterado@email.com\", \"valor\": 1100, \"parcelas\": 2, \"seguro\": false}")
		.when()
			.put("/simulacoes/44764358239")
		.then()
			.statusCode(404);
	}

	@Test
	public void t14_removerSimulacao() {
        given()
         	.pathParam("id", ID)
		.when()
			.delete("/simulacoes/{id}")
		.then()
			.body(is("OK"))
			//aqui deveria retornar 204
			.statusCode(200)
            .body(is("OK"));
	}
	
	@Test
	public void t15_removerSimulacaoNaoEncontrada() {
        given()
		.when()
			.delete("/simulacoes/1")
		.then()
			.body(is("OK"))
			//aqui deveria retornar 404 e mensagem Simula��o n�o encontrada
			.statusCode(200)
            .body(is("OK"));
	}
}
