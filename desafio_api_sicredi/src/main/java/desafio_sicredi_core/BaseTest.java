package desafio_sicredi_core;

import org.hamcrest.Matchers;
import org.junit.BeforeClass;
import io.restassured.RestAssured;
import io.restassured.builder.ResponseSpecBuilder;

public class BaseTest implements Constantes {
	
	@BeforeClass
	public static void setup( ) {
		RestAssured.baseURI = APP_URL;
		ResponseSpecBuilder resBuilder = new ResponseSpecBuilder();
		resBuilder.expectResponseTime(Matchers.lessThan(MAX_TIMEOUT));
		RestAssured.responseSpecification = resBuilder.build();
		RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
	}
}
